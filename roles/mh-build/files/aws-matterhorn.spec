%define      mh_install /opt/matterhorn
%define      __jar_repack 0

Name:        aws-matterhorn
Version:     %{mh_version}
Release:     %{dce_release}
Summary:     DCE version of Matterhorn
License:     ECL 2.0
URL:         http://opencast.org/matterhorn/
BuildRoot:   %{_tmppath}/%{name}-%{version}-%{release}
Source:      aws-matterhorn.tar.gz

Requires:    java-1.7.0-openjdk

%description 
This is AWS dce version of Matterhorn

%prep
%setup -q -n aws-matterhorn

%build

%install
rm -rf %{buildroot}
install -d -m 755 %{buildroot}%{mh_install}/bin

# start scripts
cp -r bin/matterhorn %{buildroot}%{mh_install}/bin/matterhorn
cp -r bin/matterhorn_init_d.sh %{buildroot}%{mh_install}/bin/matterhorn_init_d.sh

# application
cp -r bin/felix.jar %{buildroot}%{mh_install}/bin/felix.jar
cp -r lib %{buildroot}%{mh_install}
cp -r dce-config %{buildroot}%{mh_install}
cp -r docs %{buildroot}%{mh_install}

%clean
rm -rf %{buildroot}

%pre
# make sure there's no stale jars
rm -rf %{mh_install}/lib

%post
# make sure matterhorn user can write to %{mh_install}
chown --recursive matterhorn:matterhorn %{mh_install}
chmod 0755 %{mh_install}

%files
#%config %attr(0755,matterhorn,matterhorn) %{mh_install}/etc
%attr (0755,matterhorn,matterhorn) %{mh_install}/bin
%attr (0755,matterhorn,matterhorn) %{mh_install}/lib
%attr (0755,matterhorn,matterhorn) %{mh_install}/docs
%attr (0755,matterhorn,matterhorn) %{mh_install}/dce-config


#Uninstall
#%preun
#%postun

%changelog
* Mon Aug 18 2014 Naomi Maekawa <nmaekawa@dce.harvard.edu> - 1.1.1
- remove jars from %{mh_install}/lib
- set ownership of %{mh_install}

* Thu May 01 2014 Naomi Maekawa <nmaekawa@dce.harvard.edu> - 1.1.0
- removed all scriptlets
- rpm just dumps jars and configs to install dir
- all config is assumed to be done by provisioning

* Wed Apr 23 2014 Naomi Maekawa <nmaekawa@dce.harvard.edu> - 1.0.2
- version as defined var

* Thu Mar 06 2014 Naomi Maekawa <nmaekawa@dce.harvard.edu> - 1.0.1
- creating inbox and inbox-external in post script
- removing mh service in preun

* Thu Feb 06 2014 Naomi Maekawa <nmaekawa@dce.harvard.edu> - 1.0
-Version 1

