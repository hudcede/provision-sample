aws mh provisioning
===================

`this repo is a public sample of DCE provisioning ansible scripts. it's not being
updated nor maintained; please do not fork.`

this repo contains playbooks to generate a mh rpm from a git tag, and launch,
provision, and config mh installations.

these playbooks should be run from a build server, in aws. when launching a
mh installation, the build server must be in the same subnet as the installation
to be launched.



about the build server
======================

usually, a build server instance is launched per environment (prod|stage|dev)
and has the string "build" in its tag "Name". login into that build server.

if no build server instance is available, you can launch your own.
there should be a dce custom ami, all set up to be a build server. look for
an ami with tag Name="ami-build" under the dce-deac aws account.

launch an instance from this ami; the instance has to have around 8G mem to
run maven and compile the matterhorn modules.

if not ami-build amis are available, you will need to create one.
for more info on build servers, check the [build server repo][ami-repo].



to generate a mh rpm
====================

playbooks for rpm generation are ordered into steps:
- checkout the correct tag of matterhorn-dce-fork git repo, and setup the paella submodule
- build via maven all the available matterhorn modules
- package all relevant jars and config files for an install into a tar file
- build the rpm from the tar file and transfer it to s3

a bash wrapper *generate-mh-rpm.sh* exists for easier usage.

prerequisites
-------------
- aws access-key and secret-key (this is usually your personal keys, it is needed to talk to s3)
- matterhorn-dce-fork git tag to build rpm from

how to use
----------

    # login into the build server
    local> ssh -A ansible@<build_server>
    build>

    # set your aws credentials
    build> cd ~/provision
    build> cp private/vars/aws-creds.env ./naomi-creds.env
    build> vi naomi-creds.env # edit this file to have your access and secret key

    # run the wrapper to generate rpm
    # say you want to build from tag DCE/1.5.1-1.0.5-aws
    build> ./generate-mh-rpm.sh -c naomi.env -t DCE/1.5.1-1.0.5-aws

    # go have a coffee break because this process might take 40min or so
    # ...
    # to check that all worked, see if the correct rpm version is in s3 and has the
    # current date; the s3 bucket is mh-pkgs



to launch a mh cluster
======================


prerequisites
-------------
* aws access-key and secret-key
* aws subnet-id and security-group-id
* [aws-ami-packer repo](https://bitbucket.org/hudcede/aws-ami-packer), use this dce custom
ami to launch mh aws instances
* [aws-launch-builder](https://bitbucket.org/hudcede/aws-launch-builder), all mh instances
are to be provisioned from a build server located in the same subnet as the mh cluster
* mh internal nodes have a share nfs mount for data, this is assumed to be set before
provisioning

how to use
----------
these are step-by-step instructions on how to launch an aws matterhorn cluster.

provision scripts assume that all mh instances are already launched in aws, from a dce
custom ami (see [aws-ami-packer repo](https://bitbucket.org/hudcede/aws-ami-packer)),
which has ansible user already baked in among other dce tweaks.

a build server must also be launched (from a dce custom ami as well) in the same subnet as
the mh cluster to be deployed. from the build server, run the provision scripts.


update the inventory and prepare env vars
-----------------------------------------
we are not using dynamic inventories yet, so edit the *aws.hosts* file in the provision
repo with mh instances to be provisioned

    # login into build server launched from aws-launch-builder
    local:~ $> ssh ansible@<build-server-ip>

    # git clone the provision repo and checkout the aws branch
    build:~ $> git clone git@bitbucket.org:hudcede/provision.git
    build:~ $> cd provision
    build:provision $> git checkout aws

    # edit the aws.hosts invetory file with _private_ ips of mh instances
    build:provision $> vi aws.hosts
    ...

    # edit var env vars and source it
    # this set passwords as env vars for ansible, needed before any mh provisioning
    build:provision $> cp private/vars/mh-vars.sample mh-vars.env
    build:provision $> vi mh-vars.env
    ...
    build:provision $> source mh-vars.env


mount shared nfs
----------------
something like this is expected to be done for all mh internal, plus streaming server if one
is being used.

    # create mount point
    mh-node $> mkdir /home/data && chown matterhorn:matterhorn /home/data

    # in /etc/fstab, use something like this
    mh-node $> cat << EOM >> /etc/fstab
    10.10.10.100:/export/data  /home/data    nfs tcp,rw,hard,intr,vers=3,rsize=65535,wsize=65535,bg
    EOM


provision instances
-------------------

    # login into build server
    local:~ $> ssh ansible@<build-server-ip>

    # add ssh-key so you do not have to specify it in the command line
    build:~ $> ssh-agent bash
    build:~ $> ssh-add ~/.ssh/ansible_aws-id_rsa

    # set passwords in env vars before provisioning
    build:~ $> cd provision
    build:provision $> source mh-vars.env

    # provision db nodes
    build:provision $> ansible-playbook -i aws.hosts playbook-aws-mysql.yml --extra-vars "locale=full"

    # provision mh nodes with infrastructure (ephemeral and ebs volumes, 3rd party tools, httpd proxy, ssh keys)
    build:provision $> ansible-playbook -i aws.hosts playbook-aws-provision.yml --extra-vars "locale=full"

    # config mh nodes -- note that this example will install mh 1.5.1-1.0.6-release from s3
    build:provision $> ansible-playbook -i aws.hosts playbook-aws-mh.yml --extra-vars \
    "locale=full rpm_url=https://s3.amazonaws.com/mh-pkgs/aws-matterhorn-1.5.1-1.0.6_release.x86_64.rpm"

    # config new relic in mh nodes
    build:provision $> ansible-playbook -i aws.hosts playbook-aws-newrelic.yml --extra-vars "locale=full"

    # start mh in all nodes
    build:provision $> ansible-playbook -i aws.hosts playbook-mh-start.yml --extra-vars "locale=full"


other stuff
-----------

###add public-engage ip to list of authorized ips to query auth-daemon
the auth-daemon runs in the cm nodes, find the corresponding cm node for the enviroment
the mh cluster is being deployed and edit its _/home/conf/conf/deauth/revised_auth.conf_
to include the aws mh public engage node ip address, so it has proper access to dce
authorization.

    # edit [dev|test|prod]-dummy.fake.domain:/home/conf/deauth/revised_auth.conf
    # and add the public-engage elastic ip to the AuthMH handler
    cm.dce $> cat /home/conf/deauth/revised_auth.conf
    ...
    <Location /AuthMH>
    ...
    PerlAddVar JSONAllowedIPs <public engage elastic-ip> # aws stage-public-engage
    ...



directory organization
----------------------

    provision/
              <inventory-file>.hosts
              playbook-<playbook-file>.yml

              private/
                  files/
                      <ssh-key-files>
                  vars/
                      <role-vars>/

                          <passwd-vars>.yml

              public/
                  vars/
                      <role-vars>/
                          <general-public-vars>.yml

              roles/
                  <role-name>/
                      tasks/
                      templates/
                      files/
                      handlers/



to add playbook or inventory
----------------------------

follow the filename pattern, since they are important for selection in jenkins jobs.

    - inventory

        - extension ".hosts"
        - specify if it is an inventory for devo, stage, prod, or vagrant
        - example: mh-stage2.hosts, or jenkins-prod.hosts, or mh-vagrant-multi.hosts

    - playbook

        - prefix "playbook-"
        - example: playbook-to-install-my-silly-rpm-in-nodes-that-have-yum.yml


[ami-repo]: https://bitbucket.org/hudcede/aws-ami-packer "dce custom amis"
---eop

