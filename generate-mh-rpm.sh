#! /bin/bash

#
# wrapper for playbooks that build mh rpm:
#   1. playbook-mh-checkout-repo.yml
#   2. playbook-mh-build.yml
#   3. playbook-mh-artifacts.yml
#   4. playbook-mh-rpmbuild.yml
#   5. playbook-mh-rpm.yml <-- executes all playbooks in order
#
# 
# aws-creds-file must export aws credentials, for example:
#   export AWS_ACCESS_KEY="AKIA123"
#   export AWS_SECRET_KEY="ABCDEFGHIJKLMNOPQRSTUVXYWZ
#
# must be run from a build server
#

# initial values
PROGRAM_NAME=$(basename ${0})
RPM_WORKSPACE="$(pwd)/mh-rpm-workspace"
RPM_PLAYBOOK="playbook-mh-generate-rpm.yml"

unset MH_GIT_TAG
unset MH_VERSION
unset DCE_RELEASE
unset AWS_ACCESS_KEY
unset AWS_SECRET_KEY

#
# prints usage of this script
#
usage() {
cat << EOF
Usage: ${PROGRAM_NAME} -c <aws-creds-file> -t <mh-git-tag> -s <step>
    -c or --creds: file with aws credentials exported env vars AWS_ACCESS_KEY and AWS_SECRET_KEY
    -t or --tag: matterhorn git tag to generate the rpm from, ex: DCE/1.5.1-1.0.5-aws
    -s or --step: which step to run, values can be checkout|build|tar|rpm
        default is to run all steps to build the mh rpm
EOF
exit 1
}

#
# parses the git tag into MH_VERSION and DCE_RELEASE
#
parse_mh_tag() {
    MH_VERSION=$(echo ${1} | cut -d '/' -f 2 | awk -F'-' '{print $1}')
    if [ -z ${MH_VERSION} ]; then
        echo "unable to parse MH_VERSION from git tag ${1}. malformed?"
        usage
    fi
    DCE_RELEASE=$(echo ${1} | sed 's/-/;/' | awk -F';' '{print $2}' | sed 's/-/_/')
    if [ -z ${DCE_RELEASE} ]; then
        echo "unable to parse DCE_RELEASE from git tag ${1}. malformed?"
        usage
    fi
}


#
# main
#

# read the options
PARSED_OPTS=$(getopt -o c:t:s: --long creds:,tag:,step: -n "${PROGRAM_NAME}" -- "$@")
eval set -- "${PARSED_OPTS}"

# extract options and their arguments, plus some validation
# from http://www.bahmanm.com/blogs/command-line-options-how-to-parse-in-bash-using-getopt
while true; do
    case "${1}" in
        -c|--creds)
            case "${2}" in
                "") echo "missing aws creds file"; usage;;
                *)  if [ ! -f ${2} ]; then
                        echo "aws creds is not a file"; usage;
                    fi
                    source ${2}
                    if [ -z ${AWS_ACCESS_KEY} -o -z ${AWS_SECRET_KEY} ]; then
                        echo "missing AWS_ACCESS_KEY or AWS_SECRET_KEY as env vars"; usage
                    fi
                    shift 2;;
            esac;;
        -t|--tag)
            case "${2}" in
                "") echo "missing git tag"; usage;;
                *)  MH_GIT_TAG=${2}
                    parse_mh_tag ${MH_GIT_TAG}
                    shift 2;;
            esac;;
        -s|--step)
            case "${2}" in
                checkout) RPM_PLAYBOOK="playbook-mh-checkout.yml"; shift 2;;
                build) RPM_PLAYBOOK="playbook-mh-build.yml"; shift 2;;
                tar) RPM_PLAYBOOK="playbook-mh-tar.yml"; shift 2;;
                rpm) RPM_PLAYBOOK="playbook-mh-rpm.yml"; shift 2;;
                "") echo "missing step to execute"; usage;;
                *) echo "unknown step ${2}"; usage;;
            esac;;
        --) shift; break;;
        *) echo "unknown option: ${1}"; usage;;
    esac
done

# check required arguments
if [ -z ${MH_GIT_TAG} ]; then
    echo "missing MH_GIT_TAG"; usage
elif [ -z ${MH_VERSION} ]; then
    echo "missing MH_VERSION"; usage
elif [ -z ${DCE_RELEASE} ]; then
    echo "missing DCE_RELEASE"; usage
elif [ -z ${RPM_WORKSPACE} ]; then
    echo "missing RPM_WORKSPACE"; usage
elif [ -z ${AWS_ACCESS_KEY} -o -z ${AWS_SECRET_KEY} ]; then
    echo "missing aws credentials AWS_ACCESS_KEY|AWS_SECRET_KEY"; usage
fi

# ensure all required arguments are available as env vars
export MH_GIT_TAG
export MH_VERSION
export DCE_RELEASE
export RPM_WORKSPACE
export AWS_ACCESS_KEY
export AWS_SECRET_KEY

# run playbook
ansible-playbook -i local.hosts "${RPM_PLAYBOOK}"



